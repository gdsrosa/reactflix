//Action types
export const Types = {
  OPEN_REGISTER_VIDEO: 'ui/OPEN_REGISTER_VIDEO',
  CLOSE_REGISTER_VIDEO: 'ui/CLOSE_REGISTER_VIDEO',
};

//Reducer
const initialState = {
  isRegisterVideoFormOpen: false,
};

export const ui = (state = initialState, action) => {
  switch (action.type) {
    case Types.OPEN_REGISTER_VIDEO:
      return { ...state, isRegisterVideoFormOpen: true };
    case Types.CLOSE_REGISTER_VIDEO:
      return { ...state, isRegisterVideoFormOpen: false };
    default:
      return state;
  }
};

//Action Creators

export const openRegisterVideo = () => ({
  type: Types.OPEN_REGISTER_VIDEO,
});

export const closeRegisterVideo = () => ({
  type: Types.CLOSE_REGISTER_VIDEO,
});

//Selectors
export const getVideoRegisterFormIsOpen = state =>
  state.ui.isRegisterVideoFormOpen;
