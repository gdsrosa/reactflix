//Action Types
const Types = {
  SELECT_VIDEO_SINGLE: 'videoSingle/SELECT_VIDEO_SINGLE',
};
//Reducer
const initialState = '';

export const videoSingle = (state = initialState, action) => {
  switch (action.type) {
    case Types.SELECT_VIDEO_SINGLE:
      return action.payload.id;
    default:
      return state;
  }
};
//Action Creators
export const selectVideoSingle = id => ({
  type: Types.SELECT_VIDEO_SINGLE,
  payload: {
    id,
  },
});

//Selectors
export const getVideSingleId = state => state.videoSingle;
