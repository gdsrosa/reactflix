import { db } from '../config/firebase';

//Action Types
const Types = {
  ADD_VIDEO: 'videos/ADD_VIDEO',
};
//Reducer
const initialState = {};

export const videos = (state = initialState, action) => {
  switch (action.type) {
    case Types.ADD_VIDEO:
      return {
        ...state,
        [action.payload.id]: {
          id: action.payload.id,
          title: action.payload.title,
        },
      };
    default:
      return state;
  }
};

//Action Creators
export const registerVideoToFirebase = (id, title) => async dispatch => {
  console.log('registering video...');
  await db
    .ref('videos')
    .child(id)
    .update({
      id,
      title,
    });
  console.log('video registered!');
  dispatch(addVideo(id, title));
};

export const addVideo = (id, title) => ({
  type: Types.ADD_VIDEO,
  payload: {
    id,
    title,
  },
});

export const fetchVideos = () => dispatch => {
  db.ref('videos')
    .orderByChild('title')
    .on('child_added', child => {
      dispatch(addVideo(child.val().id, child.val().title));
    });
};

// Selectors
export const getVideos = state => state.videos;
