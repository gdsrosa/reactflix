import React from 'react';
import Home from './components/Home';
import 'normalize.css';
import 'milligram';

const App = () => {
  return <Home />;
};

export default App;
