import React from 'react';
import styled from 'styled-components';

const VideoSingle = ({ id, title }) => (
  <Container>
    <Iframe
      title="ReactConf Talk"
      width="560"
      height="450"
      src={`https://www.youtube.com/embed/${id}`}
      frameBorder="0"
      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
    />
    <Title>{title}</Title>
  </Container>
);

const Container = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

const Iframe = styled.iframe`
  background: #000;
  border: 0;
  border-bottom: 1px solid #999;
  width: 100%;
  margin-bottom: 10px;
`;

const Title = styled.h2`
  padding: 10px;
`;

export default VideoSingle;
