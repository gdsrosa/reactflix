import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Play from './play';
import { getVideos } from '../ducks/Videos';
import { selectVideoSingle } from '../ducks/VideoSingle';

const VideosList = ({ videos, handleClickOnThumb }) => (
  <Container>
    {Object.keys(videos).map(id => (
      <Video key={id}>
        <VideoLink href="/" onClick={handleClickOnThumb(id)}>
          <VideoThumb>
            <PlayStyled />
          </VideoThumb>
          <VideoTitle>{videos[id].title}</VideoTitle>
        </VideoLink>
      </Video>
    ))}
  </Container>
);

const PlayStyled = styled(Play)`
  width: 50px;
  height: 50px;
  fill: #999;
  transition: all 0.15s ease-in-out;
`;

const Video = styled.section`
  &:hover ${PlayStyled} {
    transform: scale(1.5);
  }
`;

const VideoLink = styled.a`
  color: inherit;
`;

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 10px;

  & ${Video} {
    flex: 1 1 300px;
    margin: 0 5px 5px;
  }
`;

const VideoThumb = styled.div`
  border: 1px solid #999;
  height: 150px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const VideoTitle = styled.h2`
  font-size: 18px;
`;

const mapStateToProps = state => ({
  videos: getVideos(state),
});

const mapDispatchToProps = dispatch => ({
  handleClickOnThumb: id => e => {
    e.preventDefault();
    dispatch(selectVideoSingle(id));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VideosList);
