import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import { connect } from 'react-redux';

import Footer from './Footer';
import { getVideoRegisterFormIsOpen } from '../ducks/Ui';
import { fetchVideos, getVideos } from '../ducks/Videos';
import { getVideSingleId } from '../ducks/VideoSingle';
import Header from './Header';
import VideosList from './VideosList';
import VideoSingle from './VideoSingle';
import RegisterVideo from './RegisterVideo';

class Home extends React.PureComponent {
  componentDidMount() {
    this.props.fetchVideos();
  }

  render() {
    const { isRegisterVideoFormOpen, videoSingleId, videos } = this.props;
    return (
      <Container>
        <Header />
        <Main>
          {isRegisterVideoFormOpen ? <RegisterVideo /> : ''}
          {videoSingleId ? (
            <VideoSingle
              id={videoSingleId}
              title={videos[videoSingleId].title}
            />
          ) : (
            ''
          )}
          <VideosList />
        </Main>
        <Footer />
      </Container>
    );
  }
}

const Main = styled.main`
  min-height: 100%;
`;

const Container = styled.div`
  height: 100%;
`;

createGlobalStyle`
  html, body, div[data-js="app"] {
    height: 100%
  }
`;

const mapStateToProps = state => ({
  isRegisterVideoFormOpen: getVideoRegisterFormIsOpen(state),
  videoSingleId: getVideSingleId(state),
  videos: getVideos(state),
});

const mapDispatchToProps = dispatch => ({
  fetchVideos: () => dispatch(fetchVideos()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
