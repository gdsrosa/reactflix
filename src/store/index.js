import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import { videos } from '../ducks/Videos';
import { ui } from '../ducks/Ui';
import { videoSingle } from '../ducks/VideoSingle';

const rootReducer = combineReducers({ videos, ui, videoSingle });
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(ReduxThunk))
);

export default store;
