import firebase from 'firebase';

firebase.initializeApp({
  apiKey: 'AIzaSyDPeNZsL9xURXzwNNKHx43uAlyjk_Ih03c',
  authDomain: 'reactflix-3144e.firebaseapp.com',
  databaseURL: 'https://reactflix-3144e.firebaseio.com',
  projectId: 'reactflix-3144e',
  storageBucket: 'reactflix-3144e.appspot.com',
  messagingSenderId: '724812139552',
  appId: '1:724812139552:web:710663a0df448d9a',
});

const db = firebase.database();

export { db };
